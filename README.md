# Spring Cloud 2024

尚硅谷周阳老师2024年录制的 [Spring Cloud 教程](https://www.bilibili.com/video/BV1gW421P7RD?p=1&vd_source=1cd3d6e62d1e138baf2b5d147744c9b8) 的Demo项目。

## 1、基础组件
### [Consul](https://developer.hashicorp.com/consul/docs?product_intent=consul)
服务注册和发现，动态配置和刷新。
### [OpenFeign](https://docs.spring.io/spring-cloud-openfeign/reference/spring-cloud-openfeign.html)
服务调用
### [LoadBalancer](https://docs.spring.io/spring-cloud-commons/reference/spring-cloud-commons/loadbalancer.html)
负载均衡
### [Resilience4j](https://github.com/lmhmhl/Resilience4j-Guides-Chinese/blob/main/core-modules/ratelimiter.md)
服务熔断、降级、限流
### [Micrometer](https://docs.micrometer.io/tracing/reference/) + [Zipkin](https://zipkin.io/pages/quickstart.html)
数据收集+图形化展示
## 2、Spring Cloud Alibaba组件
### [Nacos](https://nacos.io/docs/v2/what-is-nacos/)
服务注册和发现，配置中心
### [Sentinel](https://sentinelguard.io/zh-cn/docs/quick-start.html)
流量控制（限流），服务熔断和降级。可配合OpenFeign使用
### [Seata](https://seata.apache.org/docs/release-notes/)
功能分布式事务

### 项目中doc文件夹内容说明
`````` 
- bat consul持久化命令
- config seata配置
- sql 服务案例表、seata基础数据表
- file 教学中需要用到的软件包（由于文件太大已经删掉了）

``````


#### 请作者吃棒棒糖
![Buy author a sugar](https://foruda.gitee.com/images/1715237467588940812/91928680_1182656.png "微信收款码.png")
