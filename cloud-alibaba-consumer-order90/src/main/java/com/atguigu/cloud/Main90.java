package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-16 14:00
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class Main90 {
    public static void main(String[] args){
        SpringApplication.run(Main90.class, args);
        System.out.println("Hello Main90");
    }
}
