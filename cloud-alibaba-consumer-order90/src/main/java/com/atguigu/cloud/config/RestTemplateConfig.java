package com.atguigu.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-16 18:34
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    @LoadBalanced // 负载均衡必须引用的注解
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
