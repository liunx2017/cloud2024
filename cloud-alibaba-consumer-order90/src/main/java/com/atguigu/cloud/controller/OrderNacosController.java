package com.atguigu.cloud.controller;

import com.atguigu.cloud.apis.PayFeignSentinelApi;
import com.atguigu.cloud.enums.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Created with IDEA
 * @description -- alibaba的nacos测试类
 * @date 2024-04-16 18:15
 */
@RestController
@RequestMapping("order/nacos")
public class OrderNacosController {

    public static final String PaymentServerURL = "http://cloud-alibaba-payment-service";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping(value = "getInfo/{id}")
    public String getInfo(@PathVariable("id") Integer integer){
        String url = PaymentServerURL + "/pay/nacos/getInfo/" + integer;
        String data = restTemplate.getForObject(url, String.class);
        return "这是测试Nacos的消费端信息: " + data;
    }

    // 测试调用payment9001
    @Resource
    private PayFeignSentinelApi payFeignSentinelApi;
    @GetMapping("getOpenFeignSentinel/get/{id}")
    public ResultData<String> getOpenFeignSentinel(@PathVariable("id") Integer id){
        return payFeignSentinelApi.getOpenFeignSentinel(id);
    }
}
