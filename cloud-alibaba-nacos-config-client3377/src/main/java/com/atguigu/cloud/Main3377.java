package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Created with IDEA
 * @description -- 测试nacos作配置中心
 * @date 2024-04-16 19:12
 */
//@RefreshScope // 注意nacos加在这里动态刷新没有生效
@SpringBootApplication
@EnableDiscoveryClient
public class Main3377 {
    public static void main(String[] args){
        SpringApplication.run(Main3377.class, args);
        System.out.println("Hello Main3377");
    }
}
