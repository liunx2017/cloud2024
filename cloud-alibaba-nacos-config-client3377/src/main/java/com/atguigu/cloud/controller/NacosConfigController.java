package com.atguigu.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description -- nacos配置中心访问入口
 * @date 2024-04-16 19:14
 */
@RefreshScope //动态刷新注解
@RestController
@RequestMapping("nacos/config")
public class NacosConfigController {

    @Value("${config.info}")
    private String configInfo;

    // 获取nacos上面的配置信息
    @GetMapping("getNacosConfigInfo")
    public String getNacosConfigInfo(){
        return "Nacos上面的配置信息是： " + configInfo;
    }

}
