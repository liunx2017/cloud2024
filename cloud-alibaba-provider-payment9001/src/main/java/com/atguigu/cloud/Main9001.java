package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-16 11:59
 */
@SpringBootApplication
@EnableDiscoveryClient
public class Main9001 {
    public static void main(String[] args){
        SpringApplication.run(Main9001.class, args);
        System.out.println("Hello Main9001");
    }
}
