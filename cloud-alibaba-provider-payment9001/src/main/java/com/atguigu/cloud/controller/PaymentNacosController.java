package com.atguigu.cloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.enums.ReturnCodeEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-16 18:36
 */
@RestController
@RequestMapping("pay/nacos")
public class PaymentNacosController {

    @GetMapping("getInfo/{id}")
    public String getInfo(@PathVariable("id") Integer integer){
        String data= "Alibaba Nacos测试信息" ;
        return data + " ID: " + integer;
    }

    // 测试 openfeign+sentinel配合使用，实现服务降级、熔断和限流，同时解耦fallback
    @SentinelResource(value = "openFeignSentinel", blockHandler = "blockHandler"
//    ,fallback = "openFeignSentinelFallback"
    )
    @GetMapping("getOpenFeignSentinel/get/{id}")
    public ResultData<String> getOpenFeignSentinel(@PathVariable("id") Integer id){
        String data = id + "--- 测试 openfeign+sentinel配合使用，实现服务降级、熔断和限流，同时解耦fallback ---";
        return ResultData.success(data);
    }
    // 请求流量不满足限制规则异常（前端异常）
    public ResultData blockHandler(@PathVariable("id") Integer id, BlockException e){
        return ResultData.fail(ReturnCodeEnum.RC500.getCode(), id + "测试openFeignSentinel 服务不可用，触发流量规则异常： " + e.getMessage());
    }
//    // JVM中程序执行异常（后端异常）
//    // fallback服务降级方法，将统一一个全局接口，减少代码膨胀和方便管理
//    public ResultData openFeignSentinelFallback(@PathVariable("id") Integer id,Throwable throwable){
//        return ResultData.fail(ReturnCodeEnum.RC500.getCode(), id + "测试openFeignSentinel 服务不可用，程序执行异常: " + throwable.getMessage());
//    }
}
