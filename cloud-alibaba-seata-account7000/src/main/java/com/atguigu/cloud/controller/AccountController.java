package com.atguigu.cloud.controller;

import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.service.AccountService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 18:26
 */
@RestController
@RequestMapping("seata/account")
public class AccountController {

    @Resource
    private AccountService accountService;

    @PostMapping("decrease")
    public ResultData decrease(@RequestParam("userId") Long userId,
                               @RequestParam("money") Long money){
        accountService.decrease(userId, money);
        return ResultData.success("余额扣减成功");
    }



}
