package com.atguigu.cloud.service;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 18:27
 */
public interface AccountService {

    void decrease(Long userId, Long money);
}
