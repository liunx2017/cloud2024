package com.atguigu.cloud.service.impl;

import com.atguigu.cloud.mapper.AccountMapper;
import com.atguigu.cloud.service.AccountService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 18:27
 */

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountMapper accountMapper;

    @Override
    public void decrease(Long userId, Long money) {
        log.info("--- 开始扣减账户余额 ---");
        accountMapper.decrease(userId, money);

//         myTimeout();

        // int age = 10/0

        log.info("--- 结束扣减账户余额 ---");
    }


    // 模拟超时异常（openfeign超时），全局事务回滚
    private static void myTimeout(){
        try {
            TimeUnit.SECONDS.sleep(65); // 为什么是65秒？ openFeign 默认超时 60s
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
