package com.atguigu.cloud.controller;

import com.atguigu.cloud.entities.Order;
import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.service.OrderService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 18:01
 */
@RestController
@RequestMapping("seata/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("create")
    public ResultData create(Order order){
        orderService.create(order);
        return ResultData.success(order);
    }
}
