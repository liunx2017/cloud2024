package com.atguigu.cloud.service;

import com.atguigu.cloud.entities.Order;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 17:38
 */
public interface OrderService {

    void create(Order order);
}
