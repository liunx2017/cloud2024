package com.atguigu.cloud.service.impl;

import com.atguigu.cloud.apis.SeataAccountFeignApi;
import com.atguigu.cloud.apis.SeataStorageFeignApi;
import com.atguigu.cloud.entities.Order;
import com.atguigu.cloud.mapper.OrderMapper;
import com.atguigu.cloud.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 17:39
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    // 订单微服务通过feign去调用账户和库存微服务
    @Resource
    private SeataAccountFeignApi seataAccountFeignApi;
    @Resource
    private SeataStorageFeignApi seataStorageFeignApi;

    @GlobalTransactional(name = "seata-create-order", rollbackFor = Exception.class)
    @Override
    public void create(Order order) {

        // xid全局事务id的检查，重要，建议每次显示写出
        String xid = RootContext.getXID();

        log.info("--- 开始新建订单， xid= {}", xid);
        order.setStatus(0); // 注意状态，新建时设为0

        int count = orderMapper.insertSelective(order);

        // 插入成功 count == 1
        Order dbOrder = null;
        if(count > 0){
            // 创建订单成功，做相关业务
            dbOrder = orderMapper.selectOne(order);
            // 扣减库存
            seataStorageFeignApi.decrease(dbOrder.getProductId(), dbOrder.getCount());
            // 扣减余额
            seataAccountFeignApi.decrease(dbOrder.getUserId(), dbOrder.getMoney());
            // 改订单状态
            order.setStatus(1);
            // 更新订单
            Example whereCondition = new Example(Order.class);
            Example.Criteria criteria = whereCondition.createCriteria();
            criteria.andEqualTo("userId", dbOrder.getUserId());
            criteria.andEqualTo("status", 0);

            int updateCount = orderMapper.updateByExampleSelective(dbOrder, whereCondition);
            log.info("--- 修改订单完成 --- {}", updateCount);
        }
        log.info("--- 结束新建订单， xid= {}", xid);
    }
}
