package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 17:11
 */
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan("com.atguigu.cloud.mapper")
@SpringBootApplication
public class Main7002 {
    public static void main(String[] args){
        SpringApplication.run(Main7002.class, args);
        System.out.println("Hello Main7002");
    }
}
