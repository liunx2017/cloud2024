package com.atguigu.cloud.controller;

import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.service.StorageService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 18:16
 */
@RestController
@RequestMapping("seata/storage")
public class StorageController {

    @Resource
    private StorageService storageService;

    @PostMapping("decrease")
    public ResultData decrease(@RequestParam("productId") Long productId,
                               @RequestParam("count") Integer count){
        storageService.decrease(productId, count);
        return ResultData.success("库存扣减成功");
    }

}
