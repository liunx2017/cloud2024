package com.atguigu.cloud.service;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 18:13
 */
public interface StorageService {

    void decrease(Long productId, Integer count);
}
