package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Created with IDEA
 * @description -- gateway+sentinel限流网关
 * @date 2024-04-18 19:42
 */
@EnableDiscoveryClient
@SpringBootApplication
public class Main9528 {
    public static void main(String[] args){
        SpringApplication.run(Main9528.class, args);
        System.out.println("Hello Main9528");
    }
}
