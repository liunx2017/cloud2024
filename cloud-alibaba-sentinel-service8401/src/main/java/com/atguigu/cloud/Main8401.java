package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Created with IDEA
 * @description -- sentinel测试服务
 * @date 2024-04-17 10:45
 */
@EnableDiscoveryClient
@SpringBootApplication
public class Main8401 {
    public static void main(String[] args){
        SpringApplication.run(Main8401.class, args);
        System.out.println("Hello Main8401");
    }
}
