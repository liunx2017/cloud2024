package com.atguigu.cloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description -- 测试授权规则
 * @date 2024-04-18 17:16
 */
@RestController
@RequestMapping("sentinel/auth")
public class AuthController {

//    测试：授权规则 （黑白名单，IP user appName ）
    @GetMapping("testAuthRule")
    public String testAuthRule(){
        return "------------ 测试：授权规则 （黑白名单）-------------";
    }
}
