package com.atguigu.cloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.*;

/**
 * @author Created with IDEA
 * @description -- 测试@SentinelResource注解
 * @date 2024-04-18 12:20
 */
@RestController
@RequestMapping("sentinel/rateLimit")
public class RateLimitController {

    // 普通URL测试
    @GetMapping("byUrl")
    public String byUrl(){
        return "--------------RateLimit by Url---------------------";
    }

    //    哨兵模式测试
    @SentinelResource(value = "byResourceSentinelResource", blockHandler = "handleException")
    @GetMapping("byResource")
    public String byResource(){
        return "--------------RateLimit by Sentinel Resource---------------------";
    }
    public String handleException(BlockException blockException){
        return "按照资源名称SentinelResource限流测试，服务不可用触发回调";
    }

    // 加入反馈
    @SentinelResource(value = "byResourceSentinelResourceFallback", blockHandler = "handleExceptionFallback",
    fallback = "byResourceFallbackMethod")
    @GetMapping("byResourceFallback/{id}")
    public String byResourceFallback(@PathVariable("id") Integer id){
        if (id == 0){
            throw new RuntimeException("ID=0，直接抛出异常！");
        }
        return "--------------RateLimit by Sentinel Resource add Fallback Method---------------------";
    }
    public String handleExceptionFallback(@PathVariable("id") Integer id,BlockException blockException){
        return "按照资源名称SentinelResource限流测试，Sentinel配置自定义限流";
    }
    public String byResourceFallbackMethod(@PathVariable("id") Integer id, Throwable throwable){
        return "按照资源名称SentinelResource限流测试，程序逻辑异常" + throwable.getMessage();
    }

    //  测试： 热点参数限流
    @GetMapping("hot/param")
    @SentinelResource(value = "param",blockHandler = "handleHotParam")
    public String hotParam(@RequestParam(value = "param1", required = false) String param1,
                           @RequestParam(value = "param2", required = false) String param2){
        return "---------- 测试：限流热点参数，如商品ID、用户ID等---------------";
    }
    public String handleHotParam(String param1, String param2, BlockException e){
        return "---------------- 限流信息：热点参数，如商品ID、用户ID等 ---------------" + e.getMessage();
    }
}
