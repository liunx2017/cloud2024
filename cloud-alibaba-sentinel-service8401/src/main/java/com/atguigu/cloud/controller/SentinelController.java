package com.atguigu.cloud.controller;

import com.atguigu.cloud.service.FlowLimitService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author Created with IDEA
 * @description -- sentinel测试接口
 * @date 2024-04-17 10:49
 */
@RestController
@RequestMapping("sentinel")
public class SentinelController {

    @GetMapping("test/aa")
    public String testA(){
        return "------------ testAA -------------";
    }

    @GetMapping("test/bb")
    public String testB(){
        return "------------ testBB -------------";
    }

    @Resource
    private FlowLimitService flowLimitService;

    @GetMapping("test/cc")
    public String testC(){
        flowLimitService.common();
        return "------------ testCC -------------";
    }

    @GetMapping("test/dd")
    public String testD(){
        flowLimitService.common();
        return "------------ testDD -------------";
    }

    // 测试流控效果排队等待 1s一个请求，10s后作为超时处理
    @GetMapping("test/ee")
    public String testE(){
        System.out.println("流控效果：排队等待---  " + System.currentTimeMillis());
        return "------------ testEE -------------";
    }

    // 测试熔断规则-慢调用比例
    @GetMapping("test/ff")
    public String testF(){
        try {
            TimeUnit.SECONDS.sleep(1);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("测试熔断规则：慢调用比例---  " + System.currentTimeMillis());
        return "------------ testFF -------------";
    }

//    测试熔断规则：异常比例 异常数
    @GetMapping("test/gg")
    public String testG(){
        int age = 10/0;
        return "------------ testGG -------------";
    }
}
