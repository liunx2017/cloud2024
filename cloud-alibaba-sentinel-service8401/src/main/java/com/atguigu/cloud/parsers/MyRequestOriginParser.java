package com.atguigu.cloud.parsers;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

/**
 * @author Created with IDEA
 * @description -- 配置授权规则校验的参数
 * @date 2024-04-18 17:20
 */
@Component
public class MyRequestOriginParser implements RequestOriginParser {

    private final static String PARAM_KEY = "serverName";

    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getParameter(PARAM_KEY);
    }
}
