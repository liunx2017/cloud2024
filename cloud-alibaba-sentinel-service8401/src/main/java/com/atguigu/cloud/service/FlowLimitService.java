package com.atguigu.cloud.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @author Created with IDEA
 * @description -- 限流测试
 * @date 2024-04-17 14:06
 */
@Service
public class FlowLimitService {

    // 哨兵需要的资源
    @SentinelResource(value = "common")
    public void common(){
        System.out.println("---------- Flow Limit come in ----------");
    }
}
