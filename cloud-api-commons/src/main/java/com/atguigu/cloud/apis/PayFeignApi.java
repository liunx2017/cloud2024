package com.atguigu.cloud.apis;

import com.atguigu.cloud.enums.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Created with IDEA
 * @description -- 支付微服务的调用接口+URI
 * @date 2024-04-12 13:31
 */
//@FeignClient(value = "cloud-payment-service") //value写被调用服务的服务名
@FeignClient(value = "cloud-gateway") //这里添加了网关服务，同一个项目中的子项目都会走这里，如还是写直接访问的服务名，就会绕过网关
public interface PayFeignApi {

    // 所有暴露 出来的接口都可以写在这里，但为了节约时间，只写了一个。
    @GetMapping("pay/getLoadBalancerInfo")
    String getLoadBalancerInfo();

    // 测试断路器
    @GetMapping("pay/cb/{id}")
    String myCircuitBreaker(@PathVariable("id") Integer id);


    // 测试隔离
    @GetMapping("pay/cb/bulkhead/{id}")
    String myBulkhead(@PathVariable("id") Integer id);

    // 测试限流
    @GetMapping("pay/cb/rateLimiter/{id}")
    String myRateLimiter(@PathVariable("id") Integer id);


    // 测试链路追踪
    @GetMapping("pay/micrometerZipkin/info/{id}")
    ResultData<String> myMicrometerZipkin(@PathVariable("id") Integer id);

    // 测试网关
    @GetMapping("pay/gateway/get/{id}")
    ResultData<String> getById(@PathVariable("id") Integer id);

    @GetMapping("pay/gateway/getGatewayInfo")
    ResultData<String> getGatewayInfo();
}
