package com.atguigu.cloud.apis;

import com.atguigu.cloud.enums.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Created with IDEA
 * @description -- OpenFeign+Sentinel 的回调fallback统一管理接口 这里真实开发命名规则一般会：对应服务+Service
 * 这里为了配合说明命为PayFeignSentinelApi 正常走方法，异常走fallback
 * @date 2024-04-18 18:34
 */
@FeignClient(value = "cloud-alibaba-payment-service", fallback = PayFeignSentinelApiFallback.class)
public interface PayFeignSentinelApi {

    @GetMapping("/pay/nacos/getOpenFeignSentinel/get/{id}")
    ResultData<String> getOpenFeignSentinel(@PathVariable("id") Integer id);

}
