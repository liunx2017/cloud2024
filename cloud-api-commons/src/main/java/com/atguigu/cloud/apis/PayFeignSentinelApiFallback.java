package com.atguigu.cloud.apis;

import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.enums.ReturnCodeEnum;
import org.springframework.stereotype.Component;

/**
 * @author Created with IDEA
 * @description -- PayFeignSentinelApi 接口调用异常返回类
 * @date 2024-04-18 18:40
 */
@Component
public class PayFeignSentinelApiFallback implements PayFeignSentinelApi{

    @Override
    public ResultData<String> getOpenFeignSentinel(Integer id) {
        return ResultData.fail(ReturnCodeEnum.RC500.getCode(), "对方宕机或不可用，服务降级。。。");
    }
}
