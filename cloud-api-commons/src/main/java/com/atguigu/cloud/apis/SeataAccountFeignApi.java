package com.atguigu.cloud.apis;

import com.atguigu.cloud.enums.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 16:55
 */
@FeignClient(value = "cloud-alibaba-seata-account-service")
public interface SeataAccountFeignApi {

    // 扣减余额
    @PostMapping("seata/account/decrease")
    ResultData decrease(@RequestParam("userId") Long userId,
                        @RequestParam("money") Long money);
}
