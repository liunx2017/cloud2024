package com.atguigu.cloud.apis;

import com.atguigu.cloud.enums.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-20 16:54
 */
@FeignClient(value = "cloud-alibaba-seata-storage-service")
public interface SeataStorageFeignApi {

    // 扣减库存
    @PostMapping("seata/storage/decrease")
    ResultData decrease(@RequestParam("productId") Long productId,
                        @RequestParam("count") Integer count);
}
