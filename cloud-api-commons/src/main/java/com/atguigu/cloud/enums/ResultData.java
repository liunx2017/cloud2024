package com.atguigu.cloud.enums;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Created with IDEA
 * @description -- 统一返回值对象
 * @date 2024-04-09 21:15
 */
@Data
@Accessors(chain = true) // 链式编程
public class ResultData<T> {

    private String code; // 结果状态，参考ReturnCodeEnum
    private String message;

    private T data;

    private Long timestamp;

    public ResultData(){
        this.timestamp = System.currentTimeMillis();
    }

    // 成功结果
    public static <T> ResultData<T> success(T data){
        ResultData resultData = new ResultData<>();
        resultData.setCode(ReturnCodeEnum.RC200.getCode());
        resultData.setMessage(ReturnCodeEnum.RC200.getMessage());
        resultData.setData(data);
        return resultData;
    }

    // 异常结果
    public static <T> ResultData<T> fail(String code, String msg){
        ResultData resultData = new ResultData<>();
        resultData.setCode(code);
        resultData.setMessage(msg);
        return resultData;
    }
}
