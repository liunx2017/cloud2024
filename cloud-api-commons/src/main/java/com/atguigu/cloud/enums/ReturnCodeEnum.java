package com.atguigu.cloud.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * @author Created with IDEA
 * @description -- 建立枚举口诀： 举值-构造-遍历
 * @date 2024-04-09 21:00
 */
@Getter
public enum ReturnCodeEnum {

    RC999("999", "操作xxx失败"),
    RC200("200", "success"),
    RC201("201", "服务开启降级保护，请稍后再试！"),
    RC202("202", "热点参数限流，请稍后再试！"),
    RC203("203", "授权规则不通过，请稍后再试！"),
    RC375("375", "数学运算异常，请联系管理员授权！"),
    RC401("401", "匿名用户访问无权限资源时异常"),
    RC403("403", "无访问权限，请联系管理员授权！"),
    RC500("500", "系统异常，请稍后再试！");

    private final String code;
    private final String message;

    ReturnCodeEnum(String code, String message){
        this.code = code;
        this.message = message;
    }
    // 传统版
    public static ReturnCodeEnum getRCEnumV1(String code){
        for(ReturnCodeEnum e:ReturnCodeEnum.values()){
            if(e.code.equalsIgnoreCase(code)){
                return e;
            }
        }
        return null;
    }
    // 流式计算
    public static ReturnCodeEnum getRCEnumV2(String code){
        return Arrays.stream(ReturnCodeEnum.values()).filter(x -> x.getCode().equalsIgnoreCase(code)).findFirst().orElse(null);
    }
}
