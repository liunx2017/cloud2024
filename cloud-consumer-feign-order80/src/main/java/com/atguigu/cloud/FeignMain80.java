package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-12 13:18
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class FeignMain80 {

    public static void main(String[] args){
        SpringApplication.run(FeignMain80.class, args);
        System.out.println("Hello FeignMain80");
    }

}
