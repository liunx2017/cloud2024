package com.atguigu.cloud.config;

import feign.Logger;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-12 14:54
 */
@Configuration
public class FeignConfig {

    /**
     * 请求重试配置Bean
     * @return
     */
    @Bean
    public Retryer myRetryer(){
        // 默认配置，是不走重试策略的
        return Retryer.NEVER_RETRY;
        // 初始时间间隔100ms，重试时间最大间隔时间1s，最大请求数3（1默认+2重试）
//        return new Retryer.Default(100, 1, 3);
    }

    /**
     * 开启feign日志打印
     * 日志级别 NONE<BASIC<HEADERS<FULL
     * @return
     */
    @Bean
    public Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }
}
