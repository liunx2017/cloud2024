package com.atguigu.cloud.controller;

import com.atguigu.cloud.apis.PayFeignApi;
import com.atguigu.cloud.enums.ResultData;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-13 9:58
 */
@RestController
@RequestMapping("order/cb")
public class OrderCircuitBreakerController {

    @Resource
    private PayFeignApi payFeignApi;

    @CircuitBreaker(name = "cloud-payment-service", fallbackMethod = "myCircuitBreakerFallback")
    @GetMapping(value="myCircuitBreaker/{id}")
    public ResultData<String> myCircuitBreaker(@PathVariable("id") Integer id){
        String data = payFeignApi.myCircuitBreaker(id);
        return ResultData.success(data);
    }

    // 服务降级后的兜底方法myCircuitBreakerFallback
    public ResultData<String> myCircuitBreakerFallback(Throwable throwable){
        String data = "服务器繁忙，请稍后重试！";
        return ResultData.success(data);
    }

    @Bulkhead(name = "cloud-payment-service", fallbackMethod = "myCircuitBreakerFallback", type = Bulkhead.Type.SEMAPHORE)
    @GetMapping(value="myBulkhead/{id}")
    public ResultData<String> myBulkhead(@PathVariable("id") Integer id){
        String data = payFeignApi.myBulkhead(id);
        return ResultData.success(data);
    }

    // 线程池隔离测试
    @Bulkhead(name = "cloud-payment-service", fallbackMethod = "myBulkheadPoolFallback", type = Bulkhead.Type.THREADPOOL)
    @GetMapping(value="myBulkheadPool/{id}")
    public CompletableFuture<ResultData<String>> myBulkheadPool(@PathVariable("id") Integer id){
        System.out.println(Thread.currentThread().getName() + "\t" + "---开始进入");
        try{
            TimeUnit.SECONDS.sleep(3);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "\t" + "---准备离开");
        return CompletableFuture.supplyAsync(() -> {
            String data = payFeignApi.myBulkhead(id);
            return ResultData.success(data);
        });
    }

    // 兜底回调方法。注意：经验分享，这里参数保持跟主方法一致，比如主方法有参数id，下面也要写。若独一个可以不写。
    public CompletableFuture<ResultData<String>> myBulkheadPoolFallback(Integer id, Throwable throwable){
        String data = "服务器繁忙，请稍后重试！";
        return CompletableFuture.supplyAsync(() -> ResultData.success(data));
    }

    // 限流测试
    @RateLimiter(name = "cloud-payment-service", fallbackMethod = "myRateLimiterFallback")
    @GetMapping(value="myRateLimiter/{id}")
    public ResultData<String> myRateLimiter(@PathVariable("id") Integer id){
        String data = payFeignApi.myRateLimiter(id);
        return ResultData.success(data);
    }

    public ResultData<String> myRateLimiterFallback(Throwable throwable){
        String data = "你被限流了，请稍后重试！";
        return ResultData.success(data);
    }


}
