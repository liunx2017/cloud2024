package com.atguigu.cloud.controller;

import cn.hutool.core.date.DateUtil;
import com.atguigu.cloud.apis.PayFeignApi;
import com.atguigu.cloud.enums.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-12 13:37
 */
@RestController
@RequestMapping("feign/consumer")
public class OrderController {

    @Resource
    private PayFeignApi payFeignApi;

    @GetMapping(value="getLoadBalancerInfo")
    public ResultData<String> getLoadBalancerInfo(){
        String data = null;
        try {
            System.out.println("--- 服务调用开始 -------------------" + DateUtil.now());
            data = payFeignApi.getLoadBalancerInfo();
         } catch (Exception e){
            e.printStackTrace();
            System.out.println("--- 服务调用结束 -------------------" + DateUtil.now());
        }
        return ResultData.success(data);
    }
}
