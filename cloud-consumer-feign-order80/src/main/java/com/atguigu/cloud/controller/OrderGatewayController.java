package com.atguigu.cloud.controller;

import com.atguigu.cloud.apis.PayFeignApi;
import com.atguigu.cloud.enums.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-14 19:23
 */
@RestController
@RequestMapping("order/getway")
public class OrderGatewayController {

    @Resource
    private PayFeignApi payFeignApi;

    @GetMapping("pay/gateway/get/{id}")
    public ResultData<String> getById(@PathVariable("id") Integer id){

        return payFeignApi.getById(id);
    }

    @GetMapping("pay/gateway/getGatewayInfo")
    ResultData<String> getGatewayInfo(){
        return payFeignApi.getGatewayInfo() ;
    }

}
