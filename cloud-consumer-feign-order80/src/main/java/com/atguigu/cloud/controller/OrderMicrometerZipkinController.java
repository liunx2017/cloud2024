package com.atguigu.cloud.controller;

import cn.hutool.core.util.IdUtil;
import com.atguigu.cloud.apis.PayFeignApi;
import com.atguigu.cloud.enums.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-14 17:35
 */
@RestController
@RequestMapping("order/micrometerZipkin")
public class OrderMicrometerZipkinController {

    @Resource
    private PayFeignApi payFeignApi;

    @GetMapping(value = "info/{id}")
    public ResultData<String> myMicrometerZipkin(@PathVariable("id") Integer id){

        return payFeignApi.myMicrometerZipkin(id);
    }

}
