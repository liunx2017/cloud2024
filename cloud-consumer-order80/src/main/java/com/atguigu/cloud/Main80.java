package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Created with IDEA
 * @description -- Order主启动类
 * @date 2024-04-09 22:12
 */
@EnableDiscoveryClient
@SpringBootApplication
public class Main80 {

    public static void main(String[] args){
        SpringApplication.run(Main80.class, args);
        System.out.println("Hello Main80");
    }
}
