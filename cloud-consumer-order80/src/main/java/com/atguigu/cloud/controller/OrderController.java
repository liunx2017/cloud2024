package com.atguigu.cloud.controller;

import com.atguigu.cloud.entities.PayDTO;
import com.atguigu.cloud.enums.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-09 22:13
 */
@RestController
@RequestMapping(value = "consumer/order")
public class OrderController {
    // 被调用服务地址（先写死，硬编码）
//    public static final String PaymentServerURL = "http://localhost:8001";
    // 写活
    public static final String PaymentServerURL = "http://cloud-payment-service";

    @Resource
    private RestTemplate restTemplate;

    @PostMapping(value="/add")
    public ResultData add(PayDTO payDTO){
        // payment服务访问地址
        String url = PaymentServerURL + "/pay/add";
        return restTemplate.postForObject(url, payDTO, ResultData.class);
    }

    // 测试consul的分布式配置
    @GetMapping(value="/get/{id}")
    public ResultData getPayInfo(@PathVariable("id") Integer id){
        // payment服务访问地址
        String url = PaymentServerURL + "/pay/get/"+id;
        return restTemplate.getForObject(url, ResultData.class, id);
    }

    // 测试负载均衡
    @GetMapping(value="/getLoadBalancerInfo")
    public String getLoadBalancerInfo(){
        // payment服务访问地址
        String url = PaymentServerURL + "/pay/getLoadBalancerInfo";
        return restTemplate.getForObject(url, String.class);
    }
}
