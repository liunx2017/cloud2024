package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Created with IDEA
 * @description --
 * @date 2024-04-14 18:36
 */
@EnableDiscoveryClient
@SpringBootApplication
public class Main9527 {
    public static void main(String[] args){
        SpringApplication.run(Main9527.class,args);
        System.out.println("Hello Gateway9527");
    }
}
