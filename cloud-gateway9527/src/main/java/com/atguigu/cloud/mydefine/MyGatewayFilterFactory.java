package com.atguigu.cloud.mydefine;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.factory.AddRequestHeaderGatewayFilterFactory;
import org.springframework.cloud.gateway.support.GatewayToStringStyler;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

/**
 * @author Created with IDEA
 * @description -- 自定义条件过滤器
 * @date 2024-04-16 9:50
 */
@Component
public class MyGatewayFilterFactory extends AbstractGatewayFilterFactory<MyGatewayFilterFactory.Config> {

    private final static String MY_KEY = "atguigu";
    private final static String MY_SHORTCUT_KEY = "status";

    @Override
    public GatewayFilter apply(MyGatewayFilterFactory.Config config) {
        return new GatewayFilter() {
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                ServerHttpRequest request = exchange.getRequest();
                System.out.println("进入自定义条件过滤器MyGatewayFilterFactory，status=" + config.getStatus());
                // 放行
                if(request.getQueryParams().containsKey(MY_KEY)){
                    return chain.filter(exchange);
                }

                // 返回非法提示
                exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
                return exchange.getResponse().setComplete();
            }

            public String toString() {
                return GatewayToStringStyler.filterToStringCreator(MyGatewayFilterFactory.this).append(config.getStatus()).toString();
            }
        };
    }

    // 短配置方法生效
    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList(MY_SHORTCUT_KEY);
    }

    public MyGatewayFilterFactory(){
        super(MyGatewayFilterFactory.Config.class); ;
    }

    // 过滤器 规则静态内部类
    @Validated
    public static class Config {
        @Getter
        @Setter
        private String status; // 设定状态值，他等于多少，匹配才可以访问
    }
}
