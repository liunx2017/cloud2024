package com.atguigu.cloud.mydefine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author Created with IDEA
 * @description -- 自定义统计过滤器 （全局）
 * @date 2024-04-16 9:21
 */
@Slf4j
@Component
public class MyGlobalFilter implements GlobalFilter, Ordered {

    public final static String BEGIN_VISIT_TIME = "begin_visit_time";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 1 先记录访问接口的开始时间
        exchange.getAttributes().put(BEGIN_VISIT_TIME, System.currentTimeMillis());
        // 2 访问统计的各个结果给后台
        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            Long beginVisitTime = exchange.getAttribute(BEGIN_VISIT_TIME);
            if (beginVisitTime != null) {
                log.info("访问接口的主机：" + exchange.getRequest().getURI().getHost());
                log.info("访问接口的端口：" + exchange.getRequest().getURI().getPort());
                log.info("访问接口的URL：" + exchange.getRequest().getURI().getPath());
                log.info("访问接口的URL参数：" + exchange.getRequest().getURI().getRawQuery());
                log.info("访问接口的时间长短：" + (System.currentTimeMillis() - beginVisitTime) + "毫秒");
                log.info(" -----------------------------------------------------------------");
            }
        }));
    }

    // 数字越小优先级越高
    @Override
    public int getOrder() {
        return 0;
    }
}
