package com.atguigu.cloud.controller;

import cn.hutool.core.util.IdUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author Created with IDEA
 * @description -- 断路器测试控制器
 * @date 2024-04-12 20:59
 */
@RestController
@RequestMapping("pay/cb")
public class PayCircuitBreakerController {

    @GetMapping(value = "{id}")
    public String myCircuitBreaker(@PathVariable("id") Integer id){
        if (id < 0) throw new RuntimeException("ID不能为负数");
        if (id == 0){
            try {
                TimeUnit.SECONDS.sleep(5);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        return "Hello Circuit Breaker! InputId:" + id + " \t" + IdUtil.simpleUUID();
    }

    // 测试隔离bulkhead
    @GetMapping(value = "bulkhead/{id}")
    public String myBulkhead(@PathVariable("id") Integer id){
        if (id < 0) throw new RuntimeException("ID不能为负数");
        if (id == 0){
            try {
                TimeUnit.SECONDS.sleep(5);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        return "Hello Bulkhead! InputId:" + id + " \t" + IdUtil.simpleUUID();
    }

    // 测试限流RateLimiter
    @GetMapping(value = "rateLimiter/{id}")
    public String myRateLimiter(@PathVariable("id") Integer id){
        if (id < 0) throw new RuntimeException("ID不能为负数");
        if (id == 0){
            try {
                TimeUnit.SECONDS.sleep(5);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        return "Hello RateLimiter! InputId:" + id + " \t" + IdUtil.simpleUUID();
    }

}
