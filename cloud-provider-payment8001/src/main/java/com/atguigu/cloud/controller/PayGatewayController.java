package com.atguigu.cloud.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.atguigu.cloud.entities.Pay;
import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.service.PayService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Enumeration;

/**
 * @author Created with IDEA
 * @description -- 测试网关
 * @date 2024-04-14 18:49
 */
@RestController
@RequestMapping("pay/gateway")
public class PayGatewayController {

    @Resource
    private PayService payService;

    @GetMapping(value = "get/{id}")
    public ResultData<String> getById(@PathVariable("id") Integer id){
        String data = "测试网关方法之一获取支付信息";
        Pay pay = payService.getById(id);
        if (null != pay){
            data = pay.toString();
        }
        return ResultData.success(data);
    }

    @GetMapping(value = "getGatewayInfo")
    public ResultData<String> getGatewayInfo(){
        String data = "测试网关方法之二";
        data += "获取网关信息：" + IdUtil.simpleUUID();
        return ResultData.success(data);
    }

    //    获取网关有那些过滤器
    @GetMapping(value = "getFilters")
    public ResultData<String> getFilters(HttpServletRequest httpServletRequest){
        String data = "";
        Enumeration<String> headers = httpServletRequest.getHeaderNames();
        while (headers.hasMoreElements()){
            String headName = headers.nextElement();
            String headValue = httpServletRequest.getHeader(headName);
            System.out.println("请求头名称： " + headName + "\t\t\t" + "请求头值： " +headValue);
            if(headName.equalsIgnoreCase("X-Request-atguigu1")
                    || headName.equalsIgnoreCase("X-Request-atguigu2")){
                data = data + headName + "\t" + headValue + " ";
            }
        }

        System.out.println("------------------------------------------------------------");
        String memberId = httpServletRequest.getParameter("memberId");
        String memberName = httpServletRequest.getParameter("memberName");
        System.out.println("获取请求参数：" + memberId + " , " + memberName);
        System.out.println("------------------------------------------------------------");

        data += "getGatewayFilters 过滤器 Test：" + data + " \t " + DateUtil.now();
        return ResultData.success(data);
    }

}
