package com.atguigu.cloud.controller;

import cn.hutool.core.util.IdUtil;
import com.atguigu.cloud.enums.ResultData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created with IDEA
 * @description -- 测试链路追踪的入口
 * @date 2024-04-14 17:24
 */
@RestController
@RequestMapping("pay/micrometerZipkin")
public class PayMicrometerZipkinController {

    // 测试链路追踪
    @GetMapping(value = "info/{id}")
    public ResultData<String> myMicrometerZipkin(@PathVariable("id") Integer id){

        String data = "Hello Micrometer&Zipkin, InputId:" + id + "\t" + "服务返回：" + IdUtil.simpleUUID();

        return ResultData.success(data);
    }
}
