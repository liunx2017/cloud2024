package com.atguigu.cloud;

import java.time.ZonedDateTime;

/**
 * @author Created with IDEA
 * @description -- 断言中After Before Between 中用的时间格式
 * @date 2024-04-15 20:17
 */
public class TestMain {

    public static void main(String[] args){
        ZonedDateTime zoneDateTime = ZonedDateTime.now();
        System.out.println("时间： " + zoneDateTime);
    }

}
