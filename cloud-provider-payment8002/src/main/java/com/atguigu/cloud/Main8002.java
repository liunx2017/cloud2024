package com.atguigu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Created with IDEA
 * @description -- 验证负载均衡复制的 8001服务
 * @date 2024-04-09 20:01
 */
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.atguigu.cloud.mapper") //import tk.mybatis.spring.annotation.MapperScan;
public class Main8002 {
    public static void main(String[] args){
        SpringApplication.run(Main8002.class, args);
        System.out.println("Hello Main8002");
    }
}
