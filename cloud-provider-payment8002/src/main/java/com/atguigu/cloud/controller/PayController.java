package com.atguigu.cloud.controller;

import com.alibaba.fastjson2.JSON;
import com.atguigu.cloud.entities.Pay;
import com.atguigu.cloud.entities.PayDTO;
import com.atguigu.cloud.enums.ResultData;
import com.atguigu.cloud.service.PayService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author Created with IDEA
 * @description -- 支付微服务接口入口
 * @date 2024-04-09 20:16
 */
@Slf4j
@RestController
@RequestMapping(value="pay")
public class PayController {

    @Resource
    private PayService payService;

    @PostMapping(value="add")
    public ResultData<String> addPay(@RequestBody Pay pay){
        int i = payService.add(pay);
        String data =  "成功插入记录，返回值：" + i;
        return ResultData.success(data);
    }

    @DeleteMapping(value="delete/{id}")
    public ResultData<String> deletePay(@PathVariable("id") Integer id){
        int i = payService.delete(id);
        String data = "成功删除记录，返回值：" + i;
        return ResultData.success(data);
    }

    @PutMapping(value="update")
    public ResultData<String> updatePay(@RequestBody PayDTO payDTO){
        Pay pay = new Pay();
        BeanUtils.copyProperties(payDTO, pay);
        int i = payService.update(pay);
        String data =  "成功更新记录，返回值：" + i;
        return ResultData.success(data);
    }

    @GetMapping(value="get/{id}")
    public ResultData<String> getPayById(@PathVariable("id") Integer id){
        // 抛出全局异常
        if (id < 1) throw new RuntimeException("id参数错误");

//        // 不用全局异常，手动捕获异常
//        try {
//
//        }catch (Exception e){
//
//        }

        Pay pay = payService.getById(id);
        String data = "成功查询记录，返回值：" + (pay==null ? null : JSON.toJSONString(pay)) ;
        return ResultData.success(data);
    }

    //// 测试从consul拿配置内容
    @Value("${server.port}")
    private String port;
    @GetMapping(value="getConsulConfig")
    public String getConsulConfig(@Value("${atguigu.info}") String atguiguInfo){
        return "consul配置信息 atguiguInfo 返回值：" + atguiguInfo +"\t" + "端口：" +port;
    }

    // 测试负载均衡
    @Value("8002")
    private String port8002;
    @GetMapping(value="getLoadBalancerInfo")
    public String getLoadBalancerInfo(@Value("${atguigu.info}") String atguiguInfo){
        return "consul配置信息 atguiguInfo 返回值：" + atguiguInfo +"\t" + "端口：" +port8002;
    }
}
