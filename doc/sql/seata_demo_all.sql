-- ------------------------------------------------
-- # 订单
-- ------------------------------------------------

-- 1.建库 seata_order
CREATE DATABASE seata_order;

USE seata_order;

-- 2.建表 t_order
DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order`
(
    `id`     BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT(11) DEFAULT NULL COMMENT '用户ID',
    `product_id` BIGINT(11) DEFAULT NULL COMMENT '产品ID',
    `count` INT(11) DEFAULT NULL COMMENT '数量',
    `money` DECIMAL(11,0) DEFAULT NULL COMMENT '交易金额',
    `status` INT(1) DEFAULT NULL COMMENT '订单状态 0创建中1已完结',
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='订单表';

-- 3.日志表 undo_log
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT ='AT transaction mode undo table';
ALTER TABLE `undo_log` ADD INDEX `ix_log_created` (`log_created`);


-- ------------------------------------------------
-- # 账户
-- ------------------------------------------------

-- 1. 建库 seata_account
CREATE DATABASE seata_account;

USE seata_account;

-- 2.账户表
DROP TABLE IF EXISTS `t_account`;

CREATE TABLE `t_account`
(
    `id`     BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT(11) DEFAULT NULL COMMENT '用户ID',
    `total` DECIMAL(10,0) DEFAULT NULL COMMENT '总金额',
    `used` DECIMAL(10,0) DEFAULT NULL COMMENT '已用账户余额',
    `residue` DECIMAL(10,0) DEFAULT NULL COMMENT '剩余账户余额',
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='账户表';

INSERT INTO t_account(id,user_id, total, used, residue) VALUES ('1', '1', '1000', '0','1000');

-- 3.日志表 undo_log
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT ='AT transaction mode undo table';
ALTER TABLE `undo_log` ADD INDEX `ix_log_created` (`log_created`);


-- ------------------------------------------------
-- # 库存
-- ------------------------------------------------

-- 1.建库 seata_storage
CREATE DATABASE seata_storage;

USE seata_storage;

-- 2.库存表 t_storage
DROP TABLE IF EXISTS `t_storage`;

CREATE TABLE `t_storage`
(
    `id`     BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `product_id` BIGINT(11) DEFAULT NULL COMMENT '产品ID',
    `total` INT(11) DEFAULT NULL COMMENT '总库存',
    `used` INT(11) DEFAULT NULL COMMENT '已用库存',
    `residue` INT(11) DEFAULT NULL COMMENT '剩余库存',
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='库存表';

INSERT INTO t_storage(id,product_id, total, used, residue) VALUES ('1', '1', '1000', '0','1000');

-- 3.日志表 undo_log
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT ='AT transaction mode undo table';
ALTER TABLE `undo_log` ADD INDEX `ix_log_created` (`log_created`);
