//package com.atguigu.cloud.entities;
//
//import java.util.Date;
//import javax.persistence.*;
//
///**
// * 表名：t_account
//*/
//@Table(name = "t_account")
//public class Account {
//    @GeneratedValue(generator = "JDBC")
//    private Long id;
//
//    /**
//     * 用户ID
//     */
//    @Column(name = "user_id")
//    private Long userId;
//
//    /**
//     * 总金额
//     */
//    private Long total;
//
//    /**
//     * 已用账户余额
//     */
//    private Long used;
//
//    /**
//     * 剩余账户余额
//     */
//    private Long residue;
//
//    /**
//     * 创建时间
//     */
//    @Column(name = "create_time")
//    private Date createTime;
//
//    /**
//     * 更新时间
//     */
//    @Column(name = "update_time")
//    private Date updateTime;
//
//    /**
//     * @return id
//     */
//    public Long getId() {
//        return id;
//    }
//
//    /**
//     * @param id
//     */
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    /**
//     * 获取用户ID
//     *
//     * @return userId - 用户ID
//     */
//    public Long getUserId() {
//        return userId;
//    }
//
//    /**
//     * 设置用户ID
//     *
//     * @param userId 用户ID
//     */
//    public void setUserId(Long userId) {
//        this.userId = userId;
//    }
//
//    /**
//     * 获取总金额
//     *
//     * @return total - 总金额
//     */
//    public Long getTotal() {
//        return total;
//    }
//
//    /**
//     * 设置总金额
//     *
//     * @param total 总金额
//     */
//    public void setTotal(Long total) {
//        this.total = total;
//    }
//
//    /**
//     * 获取已用账户余额
//     *
//     * @return used - 已用账户余额
//     */
//    public Long getUsed() {
//        return used;
//    }
//
//    /**
//     * 设置已用账户余额
//     *
//     * @param used 已用账户余额
//     */
//    public void setUsed(Long used) {
//        this.used = used;
//    }
//
//    /**
//     * 获取剩余账户余额
//     *
//     * @return residue - 剩余账户余额
//     */
//    public Long getResidue() {
//        return residue;
//    }
//
//    /**
//     * 设置剩余账户余额
//     *
//     * @param residue 剩余账户余额
//     */
//    public void setResidue(Long residue) {
//        this.residue = residue;
//    }
//
//    /**
//     * 获取创建时间
//     *
//     * @return createTime - 创建时间
//     */
//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    /**
//     * 设置创建时间
//     *
//     * @param createTime 创建时间
//     */
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }
//
//    /**
//     * 获取更新时间
//     *
//     * @return updateTime - 更新时间
//     */
//    public Date getUpdateTime() {
//        return updateTime;
//    }
//
//    /**
//     * 设置更新时间
//     *
//     * @param updateTime 更新时间
//     */
//    public void setUpdateTime(Date updateTime) {
//        this.updateTime = updateTime;
//    }
//}
