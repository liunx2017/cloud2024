//package com.atguigu.cloud.entities;
//
//import java.util.Date;
//import javax.persistence.*;
//
///**
// * 表名：t_storage
//*/
//@Table(name = "t_storage")
//public class Storage {
//    @GeneratedValue(generator = "JDBC")
//    private Long id;
//
//    /**
//     * 产品ID
//     */
//    @Column(name = "product_id")
//    private Long productId;
//
//    /**
//     * 总库存
//     */
//    private Integer total;
//
//    /**
//     * 已用库存
//     */
//    private Integer used;
//
//    /**
//     * 剩余库存
//     */
//    private Integer residue;
//
//    /**
//     * 创建时间
//     */
//    @Column(name = "create_time")
//    private Date createTime;
//
//    /**
//     * 更新时间
//     */
//    @Column(name = "update_time")
//    private Date updateTime;
//
//    /**
//     * @return id
//     */
//    public Long getId() {
//        return id;
//    }
//
//    /**
//     * @param id
//     */
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    /**
//     * 获取产品ID
//     *
//     * @return productId - 产品ID
//     */
//    public Long getProductId() {
//        return productId;
//    }
//
//    /**
//     * 设置产品ID
//     *
//     * @param productId 产品ID
//     */
//    public void setProductId(Long productId) {
//        this.productId = productId;
//    }
//
//    /**
//     * 获取总库存
//     *
//     * @return total - 总库存
//     */
//    public Integer getTotal() {
//        return total;
//    }
//
//    /**
//     * 设置总库存
//     *
//     * @param total 总库存
//     */
//    public void setTotal(Integer total) {
//        this.total = total;
//    }
//
//    /**
//     * 获取已用库存
//     *
//     * @return used - 已用库存
//     */
//    public Integer getUsed() {
//        return used;
//    }
//
//    /**
//     * 设置已用库存
//     *
//     * @param used 已用库存
//     */
//    public void setUsed(Integer used) {
//        this.used = used;
//    }
//
//    /**
//     * 获取剩余库存
//     *
//     * @return residue - 剩余库存
//     */
//    public Integer getResidue() {
//        return residue;
//    }
//
//    /**
//     * 设置剩余库存
//     *
//     * @param residue 剩余库存
//     */
//    public void setResidue(Integer residue) {
//        this.residue = residue;
//    }
//
//    /**
//     * 获取创建时间
//     *
//     * @return createTime - 创建时间
//     */
//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    /**
//     * 设置创建时间
//     *
//     * @param createTime 创建时间
//     */
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }
//
//    /**
//     * 获取更新时间
//     *
//     * @return updateTime - 更新时间
//     */
//    public Date getUpdateTime() {
//        return updateTime;
//    }
//
//    /**
//     * 设置更新时间
//     *
//     * @param updateTime 更新时间
//     */
//    public void setUpdateTime(Date updateTime) {
//        this.updateTime = updateTime;
//    }
//}
